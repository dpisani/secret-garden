import { Scene, Vector3, DirectionalLight, Color, FogExp2 } from 'three';
import Viewer from './viewer/orbitalViewer';
import SceneManager from './managers/scene';
import SkySphere from './stage/sky';
import Floor from './stage/floor';


window.onload = () => {
  document.addEventListener('touchmove', function(e) {
    e.preventDefault();
  });
  const scene = new Scene();
  scene.background = new Color( 0xcccccc );
  scene.fog = new FogExp2( 0xcccccc, 0.002 );

  const viewer = new Viewer(scene);
  //
  // // add the sky
  const skySphere = new SkySphere();
  scene.add(skySphere.mesh);
  const sunLight = new DirectionalLight(0xffffff, 0.5);
  scene.add(sunLight);
  //
  let time = 0;
  const step = (delta) => {
    time += delta;
    const y = Math.sin(time / 50000);
    const z = Math.cos(time / 50000);
    const sunPos = new Vector3(0, y, z);

    const adjustedLight = new DirectionalLight(0xffffff, Math.sin(time / 50000) * 0.7);
    skySphere.uniforms.sunPosition.value.copy(sunPos);
    sunLight.copy(adjustedLight);
    sunLight.position.set(0, y, z);
  };

  const floor = new Floor();
  scene.add(floor.mesh);
  //
  const sceneManager = new SceneManager(scene);

  viewer.startScene(step);
  //
  // // Load JSON descriptor
  sceneManager.loadScene('../garden/landscape.json');
};
