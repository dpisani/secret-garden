export default class EntityManager {
  constructor() {
    this.entities = new Map();
    this.requested = new Map();
  }

  get(id) {
    if (!this.entities.has(id)) {
      this.entities.set(id, new Promise((resolve, reject) => {
        this.requested.set(id, { resolve, reject });
      }));
    }

    return this.entities.get(id);
  }

  fulfil(id, entity) {
    if (this.requested.has(id)) {
      const fulfilled = this.requested.get(id);
      this.entities.set(id, fulfilled);
      fulfilled.resolve(entity);
      this.requested.delete(id);
    } else {
      this.entities.set(id, new Promise((resolve) => {
        resolve(entity);
      }));
    }
  }

  deny(id) {
    if (this.requested.has(id)) {
      const rejected = this.requested.get(id);
      this.entities.set(id, rejected);
      rejected.reject();
      this.requested.delete(id);
    } else {
      this.entities.set(id, new Promise((resolve, reject) => {
        reject();
      }));
    }
  }

  process(id, descriptor) {
    this.fulfil(id, descriptor);
  }
}
