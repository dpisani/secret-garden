import { Color, AmbientLight, DirectionalLight, PointLight } from 'three';

import EntityManager from './entity-manager';

/**
    Example Usage:
    "lights": [
      {
        "type": "ambient",
        "colour": "rgb(255,255,255)",
        "intensity": 0.5
      },
      {
        "type": "directional",
        "colour": "rgb(255,255,255)",
        "intensity": 0.5,
        "direction": {
          "x": 1,
          "y": 0,
          "z": 0
        }
      },
      {
        "type": "point",
        "colour": "rgb(255,0,255)",
        "intensity": 0.5,
        "distance": 2.5,
        "position": {
          "x": 1,
          "y": 2,
          "z": 0
        }
      }
    ],
*/

export default class LightManager extends EntityManager {
    constructor(scene) {
        super();
        this.scene = scene;
    }

    process(id, descriptor) {
        const colour = new Color(descriptor.colour);
        let light;

        switch (descriptor.type) {
            case "ambient":
                light = new AmbientLight( colour, descriptor.intensity );
                break;
            case "directional":
                light = new DirectionalLight( colour, descriptor.intensity );
                light.position.set( -descriptor.direction.x,
                    -descriptor.direction.y,
                    -descriptor.direction.z );
                break;
            case "point":
                light = new PointLight( colour, descriptor.intensity, descriptor.distance );
                light.position.set( descriptor.position.x,
                    descriptor.position.y,
                    descriptor.position.z );
                break;
            default:
                this.deny(id);
                return;
        }
        this.scene.add(light);
        this.fulfil(id, light);
    }
}
