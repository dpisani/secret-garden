import { Color } from 'three';

import EntityManager from './entity-manager';
import { MeshPhongMaterial, MeshLambertMaterial, MeshStandardMaterial, MeshBasicMaterial } from 'three';

function createMaterial(typeTag, parameters) {
    switch (typeTag) {
        case "phong":
        return new MeshPhongMaterial(parameters);
        case "lambert":
        return new MeshLambertMaterial(parameters);
        case "metallic":
        return new MeshStandardMaterial(parameters);
        case "flat":
        return new MeshBasicMaterial(parameters);
    }
}

export default class MaterialManager extends EntityManager {
    constructor(textureManager) {
        super();
        this.textureManager = textureManager;
    }

    process(id, descriptor) {
        const colours = Object.entries(descriptor.colours).map((colour) => (
            [colour[0], new Color(colour[1])]
        ));

        const textureMaps = Object.entries(descriptor.maps).map((textureMap) => (
            new Promise((resolve, reject) => (
                this.textureManager.get(textureMap[1])
                .then(texture => {
                    resolve([textureMap[0], texture]);
                })
                .catch(reject)
            ))
        ));

        const parameters = Object.entries(descriptor.parameters).concat(
            textureMaps,
            colours
        );

        Promise.all(parameters)
        .then((result) => {
            const materialParams = result.reduce((params, entry) => {
                const param = {};
                param[entry[0]] = entry[1];
                return Object.assign(params, param);
            }, {});
            this.fulfil(id, createMaterial(descriptor.type, materialParams));
        })
        .catch(this.deny);
    }
}
