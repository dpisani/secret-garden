import { JSONLoader, Mesh, MeshPhongMaterial, FaceColors, DoubleSide} from 'three';
import GLTFLoader from 'imports-loader?THREE=three!exports-loader?THREE.GLTFLoader!../lib/GLTFLoader';

import EntityManager from './entity-manager';

export default class MeshManager extends EntityManager {
  constructor(loadingManager) {
    super();
    this.objectLoader = new JSONLoader(loadingManager);
    this.gltfLoader = new GLTFLoader(loadingManager);
  }

  process(id, descriptor) {
    if (descriptor.url.endsWith('.gltf')) {
      this.gltfLoader.load(descriptor.url, data => {
        this.fulfil(id, data.scene);
      },
      () => {},
      this.deny);
    } else {
      this.objectLoader.load(descriptor.url,
                (mesh, materials) => {
                  let material = materials && materials[0];
                  if (!material) {
                    material = new MeshPhongMaterial({
                      vertexColors: FaceColors,
                      side: DoubleSide,
                    });
                  }
                  this.fulfil(id, new Mesh(mesh, material));
                },
                () => {},
                this.deny
            );
    }
  }
}
