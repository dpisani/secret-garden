import { Mesh, Matrix4, Euler } from 'three';
import EntityManager from './entity-manager';

/**
    Example usage:
    "objects": [
      {
        "mesh": "sunflower",
        "material": "metal",
        "position": {
          "x": 0,
          "y": 0,
          "z": -1
        },
        "scale": {
          "x": 0.2,
          "y": 0.2,
          "z": 0.2
        },
        "rotation": {
          "x": 0.1,
          "y": 1.6,
          "z": 0
        }
      }
    ]
*/
export default class ObjectManager extends EntityManager {
  constructor(scene, meshManager, materialManager) {
    super();
    this.scene = scene;
    this.meshManager = meshManager;
    this.materialManager = materialManager;
  }

  placeObject(model, descriptor) {
    const position = descriptor.position || {x:0, y:0, z:0};
    const scale = descriptor.scale || {x:1, y:1, z:1};
    const rotation = descriptor.rotation || {x:0, y:0, z:0};

    const scaleMat = new Matrix4().makeScale(scale.x, scale.y, scale.z);
    const rotMat = new Matrix4().makeRotationFromEuler(new Euler(rotation.x, rotation.y, rotation.z, "XYZ"));
    const transMat = new Matrix4().makeTranslation(position.x, position.y, position.z);

    const mat = new Matrix4();
    mat.multiply(transMat).multiply(rotMat).multiply(scaleMat);


    model.applyMatrix(mat);
    this.scene.add(model);
  }

  process(id, descriptor) {
    this.meshManager.get(descriptor.mesh).then((mesh) => (
            mesh.clone()
        ))
        .then((object) => {
          if(descriptor.material) {
            return this.materialManager.get(descriptor.material).then((material) => {
              object.traverse((child) => {

                if ( child instanceof Mesh ) {
                  child.material = material;
                }
              });
              return object;
            });
          } else {
            return object;
          }
        })
        .then((object) => {
          this.placeObject(object, descriptor);
          this.fulfil(id, object);
        })
        .catch(this.deny);
  }
}
