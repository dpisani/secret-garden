import { LoadingManager } from 'three';

import LightManager from '../managers/lights';
import MaterialManager from '../managers/materials';
import TextureManager from '../managers/textures';
import MeshManager from '../managers/meshes';
import ObjectManager from '../managers/objects';

export default class SceneManager {
    constructor(scene) {
        this.scene = scene;
        this.loadingManager = new LoadingManager();
        this.loadingManager.onProgress = (item, loaded, total) => {
        	console.log( item, loaded, total );
        };

        this.lightManager = new LightManager(this.scene);
        this.textureManager = new TextureManager(this.loadingManager);
        this.materialManager = new MaterialManager(this.textureManager);
        this.meshManager = new MeshManager(this.loadingManager);
        this.objectManager = new ObjectManager(this.scene, this.meshManager, this.materialManager);
    }

    generateScene(descriptor) {

        Object.entries(descriptor.meshes).forEach((entry) => {
            this.meshManager.process(entry[0], entry[1]);
        });

        Object.entries(descriptor.textures).forEach((entry) => {
            this.textureManager.process(entry[0], entry[1]);
        });

        Object.entries(descriptor.materials).forEach((entry) => {
            this.materialManager.process(entry[0], entry[1]);
        });

        descriptor.objects.forEach((entry, i) => {
            this.objectManager.process(i, entry);
        });

        descriptor.lights.forEach((entry, i) => {
            this.lightManager.process(i, entry);
        });
    }

    loadScene(url) {
        fetch(url).then((response) => {
            if(response.ok) {
                response.json().then((descriptor) => {
                    this.generateScene(descriptor);
                });
            } else {
                throw new Error(`Error fetching from ${url}`);
            }
        })
        .catch((err) => {
            console.log(err);
        });
    }
}
