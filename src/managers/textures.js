import { ImageLoader, Texture } from 'three';

import EntityManager from './entity-manager';

export default class TextureManager extends EntityManager {
    constructor(loadingManager) {
        super();
        this.imageLoader = new ImageLoader(loadingManager);
    }

    process(id, descriptor) {
        const texture = new Texture();
        this.imageLoader.load(descriptor.url,
            (image) => {
                texture.image = image;
                texture.needsUpdate = true;
                this.fulfil(id, texture);
            },
            () => {},
            this.deny
        );
    }
}
