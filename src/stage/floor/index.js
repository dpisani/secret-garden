import { DoubleSide, Vector3, ShaderMaterial, CircleGeometry, Mesh } from 'three';

import floorVertexShader from './shaders/vertex.glsl';
import floorFragmentShader from './shaders/fragment.glsl';

export default class Floor {
    constructor() {
        this.uniforms = {
          luminance: { value: 1 },
          turbidity: { value: 2 },
          rayleigh: { value: 1 },
          mieCoefficient: { value: 0.005 },
          mieDirectionalG: { value: 0.8 },
          sunPosition: { value: new Vector3(0, 0.5, -1) }
        };

        const floorMat = new ShaderMaterial( {
          fragmentShader: floorFragmentShader,
          vertexShader: floorVertexShader,
          extensions: {
            derivatives: true,
          },
          side: DoubleSide,
          transparent: true,
        } );

        const floorGeo = new CircleGeometry( 30, 10 );
        this.mesh = new Mesh( floorGeo, floorMat );
        this.mesh.position.set(0, -1, 0);
        this.mesh.rotation.set(Math.PI / 2, 0, 0);

    }
}
