varying vec3 vertex;

void main() {
	vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    vertex = vec3(worldPosition.x, worldPosition.y, worldPosition.z);
}
