import { BackSide, Vector3, ShaderMaterial, SphereBufferGeometry, Mesh, MeshBasicMaterial } from 'three';

import skyVertexShader from './shaders/vertex.glsl';
import skyFragmentShader from './shaders/fragment.glsl';

export default class SkySphere {
    constructor() {
        this.uniforms = {
    		luminance: { value: 1 },
    		turbidity: { value: 2 },
    		rayleigh: { value: 1 },
    		mieCoefficient: { value: 0.005 },
    		mieDirectionalG: { value: 0.8 },
    		sunPosition: { value: new Vector3(0, 0.5, -1) }
    	};

    	var skyMat = new ShaderMaterial( {
    		fragmentShader: skyFragmentShader,
    		vertexShader: skyVertexShader,
    		uniforms: this.uniforms,
    		side: BackSide,
    	} );

    	const skyGeo = new SphereBufferGeometry( 30, 32, 15 );
    	this.mesh = new Mesh( skyGeo, skyMat );
    }
}
