import { PerspectiveCamera, Color, FogExp2 } from 'three';
import { attachRenderer } from './util';

import OrbitControls from 'imports-loader?THREE=three!exports-loader?THREE.OrbitControls!three/examples/js/controls/OrbitControls';


export default class Viewer {
  constructor(scene) {
    this.scene = scene;
    this.renderer = attachRenderer();

    // Create a three.js camera.
    this.camera = new PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 1000 );
    this.camera.position.z = 50;

    this.orbitControls = new OrbitControls( this.camera, this.renderer.domElement );

    this.lastRender = 0;
  }

  startScene(tickCallback = (() => {})) {
        // Request animation frame loop function
    const animate = (timestamp) => {
      var delta = Math.min(timestamp - this.lastRender, 500);
      this.lastRender = timestamp;
      tickCallback(delta);
      this.orbitControls.update();
      // Render the scene
      this.renderer.render(this.scene, this.camera);
      requestAnimationFrame(animate);
    };

    const handleResize = () => {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize( window.innerWidth, window.innerHeight );
    };

    requestAnimationFrame(animate);
    window.addEventListener('resize', handleResize, true);
  }
}
