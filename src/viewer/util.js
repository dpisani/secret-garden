import { WebGLRenderer } from 'three';


export const attachRenderer = () => {
    // Setup three.js WebGL renderer. Note: Antialiasing is a big performance hit.
    // Only enable it if you actually need to.
    const renderer = new WebGLRenderer({antialias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize( window.innerWidth, window.innerHeight );
    // Append the canvas element created by the renderer to document body element.
    document.body.appendChild(renderer.domElement);

    return renderer;
};
