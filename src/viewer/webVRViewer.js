import { PerspectiveCamera } from 'three';
import WebVRManager from '../lib/webvr-manager';
import { attachRenderer } from './util';

import VRControls from 'imports-loader?THREE=three!exports-loader?THREE.VRControls!three/examples/js/controls/VRControls';
import VREffect from 'imports-loader?THREE=three!exports-loader?THREE.VREffect!three/examples/js/effects/VREffect';


export default class Viewer {
    constructor(scene) {
        this.scene = scene;
        const renderer = attachRenderer();

        // Create a three.js camera.
        this.camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 10000);
        // Apply VR headset positional data to camera.
        this.controls = new VRControls(this.camera);
        this.controls.standing = true;
        // Apply VR stereo rendering to renderer.
        this.effect = new VREffect(renderer);
        this.effect.setSize(window.innerWidth, window.innerHeight);
        // Create a VR manager helper to enter and exit VR mode.
        const params = {
          hideButton: false, // Default: false.
          isUndistorted: false // Default: false.
        };
        this.manager = new WebVRManager(renderer, this.effect, params);

        this.lastRender = 0;
    }

    startScene(tickCallback = (() => {})) {
        // Request animation frame loop function
        const animate = (timestamp) => {
              var delta = Math.min(timestamp - this.lastRender, 500);
              this.lastRender = timestamp;
              this.controls.update();
              tickCallback(delta);
              // Render the scene through the manager.
              this.manager.render(this.scene, this.camera, timestamp);
              requestAnimationFrame(animate);
          };

          const handleResize = () => {
              this.effect.setSize(window.innerWidth, window.innerHeight);
              this.camera.aspect = window.innerWidth / window.innerHeight;
              this.camera.updateProjectionMatrix();
          };

        requestAnimationFrame(animate);
        window.addEventListener('resize', handleResize, true);
        window.addEventListener('vrdisplaypresentchange', handleResize, true);
    }
}
