var webpack = require('webpack');

var config = {
  context: __dirname + '/src', // `__dirname` is root of project and `src` is source
  entry: {
    vendor: ['three'],
    app: './app.js',
    fixedView: './fixed-view.js',
  },
  output: {
    path: __dirname + '/dist', // `dist` is the destination
    publicPath: "/assets/",
    filename: '[name].js',
  },
  devServer: {
    open: true, // to open the local server in browser
    contentBase: __dirname + '/src',
    inline: true,
  },
  devtool: "eval-source-map", // Default development sourcemap
  module: {
    rules: [
      {
        test: /\.js$/, //Check for all js files
        use: [{
          loader: 'babel-loader',
          options: { presets: ['es2015'] }
        }]
      },
      {
        test: /\.glsl$/, //Check for all js files
        use: [{
          loader: 'raw-loader',
        }]
      }
    ]
  },
  resolve: {
    alias: {
      threeExamples: __dirname+'/node_modules/three/examples/js'
    }
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",

      filename: "vendor.js",
            // (Give the chunk a different name)

      minChunks: Infinity,
            // (with more entries, this ensures that no other module
            //  goes into the vendor chunk)
    })
  ]
};

// Check if build is running in production mode, then change the sourcemap type
if (process.env.NODE_ENV === "production") {
  config.devtool = "source-map";
}

module.exports = config;
